
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/__qc_index__.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}
require('./assets/migration/use_v2.0.x_cc.Toggle_event');
require('./assets/scripts/Game');
require('./assets/scripts/Player');
require('./assets/scripts/Star');

                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Star.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '87585pBUQdH77x5N1UvgzqF', 'Star');
// scripts/Star.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    // 这个属性引用了星星预制资源
    starPrefab: {
      "default": null,
      type: cc.Prefab
    },
    // 星星产生后消失时间的随机范围
    maxStarDuration: 0,
    minStarDuration: 0,
    // 地面节点，用于确定星星生成的高度
    ground: {
      "default": null,
      type: cc.Node
    },
    // player 节点，用于获取主角弹跳的高度，和控制主角行动开关
    player: {
      "default": null,
      type: cc.Node
    }
  },
  // LIFE-CYCLE CALLBACKS:
  // onLoad () {},
  start: function start() {} // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL1N0YXIuanMiXSwibmFtZXMiOlsiY2MiLCJDbGFzcyIsIkNvbXBvbmVudCIsInByb3BlcnRpZXMiLCJzdGFyUHJlZmFiIiwidHlwZSIsIlByZWZhYiIsIm1heFN0YXJEdXJhdGlvbiIsIm1pblN0YXJEdXJhdGlvbiIsImdyb3VuZCIsIk5vZGUiLCJwbGF5ZXIiLCJzdGFydCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFO0FBQ1o7QUFDQUMsSUFBQUEsVUFBVSxFQUFFO0FBQ1YsaUJBQVMsSUFEQztBQUVWQyxNQUFBQSxJQUFJLEVBQUVMLEVBQUUsQ0FBQ007QUFGQyxLQUZBO0FBTVo7QUFDQUMsSUFBQUEsZUFBZSxFQUFFLENBUEw7QUFRWkMsSUFBQUEsZUFBZSxFQUFFLENBUkw7QUFTWjtBQUNBQyxJQUFBQSxNQUFNLEVBQUU7QUFDTixpQkFBUyxJQURIO0FBRU5KLE1BQUFBLElBQUksRUFBRUwsRUFBRSxDQUFDVTtBQUZILEtBVkk7QUFjWjtBQUNBQyxJQUFBQSxNQUFNLEVBQUU7QUFDTixpQkFBUyxJQURIO0FBRU5OLE1BQUFBLElBQUksRUFBRUwsRUFBRSxDQUFDVTtBQUZIO0FBZkksR0FIUDtBQXdCTDtBQUVBO0FBRUFFLEVBQUFBLEtBNUJLLG1CQTRCSSxDQUVSLENBOUJJLENBZ0NMOztBQWhDSyxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBjYy5DbGFzczpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2NsYXNzLmh0bWxcbi8vIExlYXJuIEF0dHJpYnV0ZTpcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcbi8vIExlYXJuIGxpZmUtY3ljbGUgY2FsbGJhY2tzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxuXG5jYy5DbGFzcyh7XG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxuXG4gICAgcHJvcGVydGllczoge1xuXHRcdFx0XHQvLyDov5nkuKrlsZ7mgKflvJXnlKjkuobmmJ/mmJ/pooTliLbotYTmupBcblx0XHRcdFx0c3RhclByZWZhYjoge1xuXHRcdFx0XHRcdFx0ZGVmYXVsdDogbnVsbCxcblx0XHRcdFx0XHRcdHR5cGU6IGNjLlByZWZhYlxuXHRcdFx0XHR9LFxuXHRcdFx0XHQvLyDmmJ/mmJ/kuqfnlJ/lkI7mtojlpLHml7bpl7TnmoTpmo/mnLrojIPlm7Rcblx0XHRcdFx0bWF4U3RhckR1cmF0aW9uOiAwLFxuXHRcdFx0XHRtaW5TdGFyRHVyYXRpb246IDAsXG5cdFx0XHRcdC8vIOWcsOmdouiKgueCue+8jOeUqOS6juehruWumuaYn+aYn+eUn+aIkOeahOmrmOW6plxuXHRcdFx0XHRncm91bmQ6IHtcblx0XHRcdFx0XHRcdGRlZmF1bHQ6IG51bGwsXG5cdFx0XHRcdFx0XHR0eXBlOiBjYy5Ob2RlXG5cdFx0XHRcdH0sXG5cdFx0XHRcdC8vIHBsYXllciDoioLngrnvvIznlKjkuo7ojrflj5bkuLvop5LlvLnot7PnmoTpq5jluqbvvIzlkozmjqfliLbkuLvop5LooYzliqjlvIDlhbNcblx0XHRcdFx0cGxheWVyOiB7XG5cdFx0XHRcdFx0XHRkZWZhdWx0OiBudWxsLFxuXHRcdFx0XHRcdFx0dHlwZTogY2MuTm9kZVxuXHRcdFx0XHR9XG5cdFx0fSxcblxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgLy8gb25Mb2FkICgpIHt9LFxuXG4gICAgc3RhcnQgKCkge1xuXG4gICAgfSxcblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9LFxufSk7XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/migration/use_v2.0.x_cc.Toggle_event.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'bc9ad7HNhRCMY9Mzs0YhFf6', 'use_v2.0.x_cc.Toggle_event');
// migration/use_v2.0.x_cc.Toggle_event.js

"use strict";

/*
 * This script is automatically generated by Cocos Creator and is only compatible with projects prior to v2.1.0.
 * You do not need to manually add this script in any other project.
 * If you don't use cc.Toggle in your project, you can delete this script directly.
 * If your project is hosted in VCS such as git, submit this script together.
 *
 * 此脚本由 Cocos Creator 自动生成，仅用于兼容 v2.1.0 之前版本的工程，
 * 你无需在任何其它项目中手动添加此脚本。
 * 如果你的项目中没用到 Toggle，可直接删除该脚本。
 * 如果你的项目有托管于 git 等版本库，请将此脚本一并上传。
 */
if (cc.Toggle) {
  // Whether the 'toggle' and 'checkEvents' events are fired when 'toggle.check() / toggle.uncheck()' is called in the code
  // 在代码中调用 'toggle.check() / toggle.uncheck()' 时是否触发 'toggle' 与 'checkEvents' 事件
  cc.Toggle._triggerEventInScript_check = true;
}

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9taWdyYXRpb24vdXNlX3YyLjAueF9jYy5Ub2dnbGVfZXZlbnQuanMiXSwibmFtZXMiOlsiY2MiLCJUb2dnbGUiLCJfdHJpZ2dlckV2ZW50SW5TY3JpcHRfY2hlY2siXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7Ozs7Ozs7Ozs7O0FBWUEsSUFBSUEsRUFBRSxDQUFDQyxNQUFQLEVBQWU7QUFDWDtBQUNBO0FBQ0FELEVBQUFBLEVBQUUsQ0FBQ0MsTUFBSCxDQUFVQywyQkFBVixHQUF3QyxJQUF4QztBQUNIIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvKlxuICogVGhpcyBzY3JpcHQgaXMgYXV0b21hdGljYWxseSBnZW5lcmF0ZWQgYnkgQ29jb3MgQ3JlYXRvciBhbmQgaXMgb25seSBjb21wYXRpYmxlIHdpdGggcHJvamVjdHMgcHJpb3IgdG8gdjIuMS4wLlxuICogWW91IGRvIG5vdCBuZWVkIHRvIG1hbnVhbGx5IGFkZCB0aGlzIHNjcmlwdCBpbiBhbnkgb3RoZXIgcHJvamVjdC5cbiAqIElmIHlvdSBkb24ndCB1c2UgY2MuVG9nZ2xlIGluIHlvdXIgcHJvamVjdCwgeW91IGNhbiBkZWxldGUgdGhpcyBzY3JpcHQgZGlyZWN0bHkuXG4gKiBJZiB5b3VyIHByb2plY3QgaXMgaG9zdGVkIGluIFZDUyBzdWNoIGFzIGdpdCwgc3VibWl0IHRoaXMgc2NyaXB0IHRvZ2V0aGVyLlxuICpcbiAqIOatpOiEmuacrOeUsSBDb2NvcyBDcmVhdG9yIOiHquWKqOeUn+aIkO+8jOS7heeUqOS6juWFvOWuuSB2Mi4xLjAg5LmL5YmN54mI5pys55qE5bel56iL77yMXG4gKiDkvaDml6DpnIDlnKjku7vkvZXlhbblroPpobnnm67kuK3miYvliqjmt7vliqDmraTohJrmnKzjgIJcbiAqIOWmguaenOS9oOeahOmhueebruS4reayoeeUqOWIsCBUb2dnbGXvvIzlj6/nm7TmjqXliKDpmaTor6XohJrmnKzjgIJcbiAqIOWmguaenOS9oOeahOmhueebruacieaJmOeuoeS6jiBnaXQg562J54mI5pys5bqT77yM6K+35bCG5q2k6ISa5pys5LiA5bm25LiK5Lyg44CCXG4gKi9cblxuaWYgKGNjLlRvZ2dsZSkge1xuICAgIC8vIFdoZXRoZXIgdGhlICd0b2dnbGUnIGFuZCAnY2hlY2tFdmVudHMnIGV2ZW50cyBhcmUgZmlyZWQgd2hlbiAndG9nZ2xlLmNoZWNrKCkgLyB0b2dnbGUudW5jaGVjaygpJyBpcyBjYWxsZWQgaW4gdGhlIGNvZGVcbiAgICAvLyDlnKjku6PnoIHkuK3osIPnlKggJ3RvZ2dsZS5jaGVjaygpIC8gdG9nZ2xlLnVuY2hlY2soKScg5pe25piv5ZCm6Kem5Y+RICd0b2dnbGUnIOS4jiAnY2hlY2tFdmVudHMnIOS6i+S7tlxuICAgIGNjLlRvZ2dsZS5fdHJpZ2dlckV2ZW50SW5TY3JpcHRfY2hlY2sgPSB0cnVlO1xufVxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Player.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'f9223NU/XBFkJcxonbQhkp1', 'Player');
// scripts/Player.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
//cc 是 Cocos 的简称，Cocos 引擎的主要命名空间，引擎代码中所有的类、函数、属性和常量都在这个命名空间中定义
cc.Class({
  "extends": cc.Component,
  properties: {
    // 主角跳跃高度
    jumpHeight: 100,
    // 主角跳跃持续时间
    jumpDuration: 0.3,
    // 最大移动速度
    maxMoveSpeed: 400,
    // 加速度
    accel: 1000
  },
  setJumpAction: function setJumpAction() {
    // 跳跃上升
    //moveBy() 方法的作用是在规定的时间内移动指定的一段距离，第一个参数就是我们之前定义主角属性中的跳跃时间，第二个参数是一个 Vec2（表示 2D 向量和坐标）类型的对象，
    var jumpUp = cc.moveBy(this.jumpDuration, cc.v2(0, this.jumpHeight)).easing(cc.easeCubicActionOut()); // 下落

    var jumpDown = cc.moveBy(this.jumpDuration, cc.v2(0, -this.jumpHeight)).easing(cc.easeCubicActionIn()); // 不断重复

    return cc.repeatForever(cc.sequence(jumpUp, jumpDown));
  },
  onKeyDown: function onKeyDown(event) {
    // set a flag when key pressed
    switch (event.keyCode) {
      case cc.macro.KEY.a:
        this.accLeft = true;
        break;

      case cc.macro.KEY.d:
        this.accRight = true;
        break;
    }
  },
  onKeyUp: function onKeyUp(event) {
    // unset a flag when key released
    switch (event.keyCode) {
      case cc.macro.KEY.a:
        this.accLeft = false;
        break;

      case cc.macro.KEY.d:
        this.accRight = false;
        break;
    }
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    // 初始化跳跃动作
    this.jumpAction = this.setJumpAction();
    this.node.runAction(this.jumpAction); // 加速度方向开关

    this.accLeft = false;
    this.accRight = false; // 主角当前水平方向速度

    this.xSpeed = 0; // 初始化键盘输入监听

    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
  },
  onDestroy: function onDestroy() {
    // 取消键盘输入监听
    cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
  },
  start: function start() {},
  update: function update(dt) {
    // 根据当前加速度方向每帧更新速度
    if (this.accLeft) {
      this.xSpeed -= this.accel * dt;
    } else if (this.accRight) {
      this.xSpeed += this.accel * dt;
    } // 限制主角的速度不能超过最大值


    if (Math.abs(this.xSpeed) > this.maxMoveSpeed) {
      // if speed reach limit, use max speed with current direction
      this.xSpeed = this.maxMoveSpeed * this.xSpeed / Math.abs(this.xSpeed);
    } // 根据当前速度更新主角的位置


    this.node.x += this.xSpeed * dt;
  } // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL1BsYXllci5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsImp1bXBIZWlnaHQiLCJqdW1wRHVyYXRpb24iLCJtYXhNb3ZlU3BlZWQiLCJhY2NlbCIsInNldEp1bXBBY3Rpb24iLCJqdW1wVXAiLCJtb3ZlQnkiLCJ2MiIsImVhc2luZyIsImVhc2VDdWJpY0FjdGlvbk91dCIsImp1bXBEb3duIiwiZWFzZUN1YmljQWN0aW9uSW4iLCJyZXBlYXRGb3JldmVyIiwic2VxdWVuY2UiLCJvbktleURvd24iLCJldmVudCIsImtleUNvZGUiLCJtYWNybyIsIktFWSIsImEiLCJhY2NMZWZ0IiwiZCIsImFjY1JpZ2h0Iiwib25LZXlVcCIsIm9uTG9hZCIsImp1bXBBY3Rpb24iLCJub2RlIiwicnVuQWN0aW9uIiwieFNwZWVkIiwic3lzdGVtRXZlbnQiLCJvbiIsIlN5c3RlbUV2ZW50IiwiRXZlbnRUeXBlIiwiS0VZX0RPV04iLCJLRVlfVVAiLCJvbkRlc3Ryb3kiLCJvZmYiLCJzdGFydCIsInVwZGF0ZSIsImR0IiwiTWF0aCIsImFicyIsIngiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFO0FBQ1o7QUFDQUMsSUFBQUEsVUFBVSxFQUFFLEdBRkE7QUFHWjtBQUNBQyxJQUFBQSxZQUFZLEVBQUUsR0FKRjtBQUtaO0FBQ0FDLElBQUFBLFlBQVksRUFBRSxHQU5GO0FBT1o7QUFDQUMsSUFBQUEsS0FBSyxFQUFFO0FBUkssR0FIUDtBQWFQQyxFQUFBQSxhQUFhLEVBQUUseUJBQVk7QUFDekI7QUFDQTtBQUNBLFFBQUlDLE1BQU0sR0FBR1QsRUFBRSxDQUFDVSxNQUFILENBQVUsS0FBS0wsWUFBZixFQUE2QkwsRUFBRSxDQUFDVyxFQUFILENBQU0sQ0FBTixFQUFTLEtBQUtQLFVBQWQsQ0FBN0IsRUFBd0RRLE1BQXhELENBQStEWixFQUFFLENBQUNhLGtCQUFILEVBQS9ELENBQWIsQ0FIeUIsQ0FJekI7O0FBQ0EsUUFBSUMsUUFBUSxHQUFHZCxFQUFFLENBQUNVLE1BQUgsQ0FBVSxLQUFLTCxZQUFmLEVBQTZCTCxFQUFFLENBQUNXLEVBQUgsQ0FBTSxDQUFOLEVBQVMsQ0FBQyxLQUFLUCxVQUFmLENBQTdCLEVBQXlEUSxNQUF6RCxDQUFnRVosRUFBRSxDQUFDZSxpQkFBSCxFQUFoRSxDQUFmLENBTHlCLENBTXpCOztBQUNBLFdBQU9mLEVBQUUsQ0FBQ2dCLGFBQUgsQ0FBaUJoQixFQUFFLENBQUNpQixRQUFILENBQVlSLE1BQVosRUFBb0JLLFFBQXBCLENBQWpCLENBQVA7QUFDRCxHQXJCTTtBQXNCUEksRUFBQUEsU0F0Qk8scUJBc0JJQyxLQXRCSixFQXNCVztBQUNoQjtBQUNBLFlBQU9BLEtBQUssQ0FBQ0MsT0FBYjtBQUNFLFdBQUtwQixFQUFFLENBQUNxQixLQUFILENBQVNDLEdBQVQsQ0FBYUMsQ0FBbEI7QUFDRSxhQUFLQyxPQUFMLEdBQWUsSUFBZjtBQUNBOztBQUNGLFdBQUt4QixFQUFFLENBQUNxQixLQUFILENBQVNDLEdBQVQsQ0FBYUcsQ0FBbEI7QUFDRSxhQUFLQyxRQUFMLEdBQWdCLElBQWhCO0FBQ0E7QUFOSjtBQVFELEdBaENNO0FBa0NQQyxFQUFBQSxPQWxDTyxtQkFrQ0VSLEtBbENGLEVBa0NTO0FBQ2Q7QUFDQSxZQUFPQSxLQUFLLENBQUNDLE9BQWI7QUFDRSxXQUFLcEIsRUFBRSxDQUFDcUIsS0FBSCxDQUFTQyxHQUFULENBQWFDLENBQWxCO0FBQ0UsYUFBS0MsT0FBTCxHQUFlLEtBQWY7QUFDQTs7QUFDRixXQUFLeEIsRUFBRSxDQUFDcUIsS0FBSCxDQUFTQyxHQUFULENBQWFHLENBQWxCO0FBQ0UsYUFBS0MsUUFBTCxHQUFnQixLQUFoQjtBQUNBO0FBTko7QUFRRCxHQTVDTTtBQTZDTDtBQUVBRSxFQUFBQSxNQUFNLEVBQUUsa0JBQVk7QUFDcEI7QUFDQSxTQUFLQyxVQUFMLEdBQWtCLEtBQUtyQixhQUFMLEVBQWxCO0FBQ0EsU0FBS3NCLElBQUwsQ0FBVUMsU0FBVixDQUFvQixLQUFLRixVQUF6QixFQUhvQixDQUlwQjs7QUFDQSxTQUFLTCxPQUFMLEdBQWUsS0FBZjtBQUNBLFNBQUtFLFFBQUwsR0FBZ0IsS0FBaEIsQ0FOb0IsQ0FPcEI7O0FBQ0EsU0FBS00sTUFBTCxHQUFjLENBQWQsQ0FSb0IsQ0FVcEI7O0FBQ0FoQyxJQUFBQSxFQUFFLENBQUNpQyxXQUFILENBQWVDLEVBQWYsQ0FBa0JsQyxFQUFFLENBQUNtQyxXQUFILENBQWVDLFNBQWYsQ0FBeUJDLFFBQTNDLEVBQXFELEtBQUtuQixTQUExRCxFQUFxRSxJQUFyRTtBQUNBbEIsSUFBQUEsRUFBRSxDQUFDaUMsV0FBSCxDQUFlQyxFQUFmLENBQWtCbEMsRUFBRSxDQUFDbUMsV0FBSCxDQUFlQyxTQUFmLENBQXlCRSxNQUEzQyxFQUFtRCxLQUFLWCxPQUF4RCxFQUFpRSxJQUFqRTtBQUNELEdBNURNO0FBNkRQWSxFQUFBQSxTQTdETyx1QkE2RE07QUFDWDtBQUNBdkMsSUFBQUEsRUFBRSxDQUFDaUMsV0FBSCxDQUFlTyxHQUFmLENBQW1CeEMsRUFBRSxDQUFDbUMsV0FBSCxDQUFlQyxTQUFmLENBQXlCQyxRQUE1QyxFQUFzRCxLQUFLbkIsU0FBM0QsRUFBc0UsSUFBdEU7QUFDQWxCLElBQUFBLEVBQUUsQ0FBQ2lDLFdBQUgsQ0FBZU8sR0FBZixDQUFtQnhDLEVBQUUsQ0FBQ21DLFdBQUgsQ0FBZUMsU0FBZixDQUF5QkUsTUFBNUMsRUFBb0QsS0FBS1gsT0FBekQsRUFBa0UsSUFBbEU7QUFDRCxHQWpFTTtBQWtFTGMsRUFBQUEsS0FsRUssbUJBa0VJLENBRVIsQ0FwRUk7QUFxRVBDLEVBQUFBLE1BQU0sRUFBRSxnQkFBVUMsRUFBVixFQUFjO0FBQ3BCO0FBQ0EsUUFBSSxLQUFLbkIsT0FBVCxFQUFrQjtBQUNoQixXQUFLUSxNQUFMLElBQWUsS0FBS3pCLEtBQUwsR0FBYW9DLEVBQTVCO0FBQ0QsS0FGRCxNQUVPLElBQUksS0FBS2pCLFFBQVQsRUFBbUI7QUFDeEIsV0FBS00sTUFBTCxJQUFlLEtBQUt6QixLQUFMLEdBQWFvQyxFQUE1QjtBQUNELEtBTm1CLENBT3BCOzs7QUFDQSxRQUFLQyxJQUFJLENBQUNDLEdBQUwsQ0FBUyxLQUFLYixNQUFkLElBQXdCLEtBQUsxQixZQUFsQyxFQUFpRDtBQUMvQztBQUNBLFdBQUswQixNQUFMLEdBQWMsS0FBSzFCLFlBQUwsR0FBb0IsS0FBSzBCLE1BQXpCLEdBQWtDWSxJQUFJLENBQUNDLEdBQUwsQ0FBUyxLQUFLYixNQUFkLENBQWhEO0FBQ0QsS0FYbUIsQ0FhcEI7OztBQUNBLFNBQUtGLElBQUwsQ0FBVWdCLENBQVYsSUFBZSxLQUFLZCxNQUFMLEdBQWNXLEVBQTdCO0FBQ0QsR0FwRk0sQ0FzRkw7O0FBdEZLLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIGNjLkNsYXNzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvY2xhc3MuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG4vL2NjIOaYryBDb2NvcyDnmoTnroDnp7DvvIxDb2NvcyDlvJXmk47nmoTkuLvopoHlkb3lkI3nqbrpl7TvvIzlvJXmk47ku6PnoIHkuK3miYDmnInnmoTnsbvjgIHlh73mlbDjgIHlsZ7mgKflkozluLjph4/pg73lnKjov5nkuKrlkb3lkI3nqbrpl7TkuK3lrprkuYlcbmNjLkNsYXNzKHtcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXG5cbiAgICBwcm9wZXJ0aWVzOiB7XG5cdFx0XHRcdC8vIOS4u+inkui3s+i3g+mrmOW6plxuXHRcdFx0XHRqdW1wSGVpZ2h0OiAxMDAsXG5cdFx0XHRcdC8vIOS4u+inkui3s+i3g+aMgee7reaXtumXtFxuXHRcdFx0XHRqdW1wRHVyYXRpb246IDAuMyxcblx0XHRcdFx0Ly8g5pyA5aSn56e75Yqo6YCf5bqmXG5cdFx0XHRcdG1heE1vdmVTcGVlZDogNDAwLFxuXHRcdFx0XHQvLyDliqDpgJ/luqZcblx0XHRcdFx0YWNjZWw6IDEwMDAsXG5cdFx0fSxcblx0XHRzZXRKdW1wQWN0aW9uOiBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdC8vIOi3s+i3g+S4iuWNh1xuXHRcdFx0XHQvL21vdmVCeSgpIOaWueazleeahOS9nOeUqOaYr+WcqOinhOWumueahOaXtumXtOWGheenu+WKqOaMh+WumueahOS4gOautei3neemu++8jOesrOS4gOS4quWPguaVsOWwseaYr+aIkeS7rOS5i+WJjeWumuS5ieS4u+inkuWxnuaAp+S4reeahOi3s+i3g+aXtumXtO+8jOesrOS6jOS4quWPguaVsOaYr+S4gOS4qiBWZWMy77yI6KGo56S6IDJEIOWQkemHj+WSjOWdkOagh++8ieexu+Wei+eahOWvueixoe+8jFxuXHRcdFx0XHR2YXIganVtcFVwID0gY2MubW92ZUJ5KHRoaXMuanVtcER1cmF0aW9uLCBjYy52MigwLCB0aGlzLmp1bXBIZWlnaHQpKS5lYXNpbmcoY2MuZWFzZUN1YmljQWN0aW9uT3V0KCkpO1xuXHRcdFx0XHQvLyDkuIvokL1cblx0XHRcdFx0dmFyIGp1bXBEb3duID0gY2MubW92ZUJ5KHRoaXMuanVtcER1cmF0aW9uLCBjYy52MigwLCAtdGhpcy5qdW1wSGVpZ2h0KSkuZWFzaW5nKGNjLmVhc2VDdWJpY0FjdGlvbkluKCkpO1xuXHRcdFx0XHQvLyDkuI3mlq3ph43lpI1cblx0XHRcdFx0cmV0dXJuIGNjLnJlcGVhdEZvcmV2ZXIoY2Muc2VxdWVuY2UoanVtcFVwLCBqdW1wRG93bikpO1xuXHRcdH0sXG5cdFx0b25LZXlEb3duIChldmVudCkge1xuXHRcdFx0XHQvLyBzZXQgYSBmbGFnIHdoZW4ga2V5IHByZXNzZWRcblx0XHRcdFx0c3dpdGNoKGV2ZW50LmtleUNvZGUpIHtcblx0XHRcdFx0XHRcdGNhc2UgY2MubWFjcm8uS0VZLmE6XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5hY2NMZWZ0ID0gdHJ1ZTtcblx0XHRcdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRcdGNhc2UgY2MubWFjcm8uS0VZLmQ6XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5hY2NSaWdodCA9IHRydWU7XG5cdFx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdH1cblx0XHR9LFxuXG5cdFx0b25LZXlVcCAoZXZlbnQpIHtcblx0XHRcdFx0Ly8gdW5zZXQgYSBmbGFnIHdoZW4ga2V5IHJlbGVhc2VkXG5cdFx0XHRcdHN3aXRjaChldmVudC5rZXlDb2RlKSB7XG5cdFx0XHRcdFx0XHRjYXNlIGNjLm1hY3JvLktFWS5hOlxuXHRcdFx0XHRcdFx0XHRcdHRoaXMuYWNjTGVmdCA9IGZhbHNlO1xuXHRcdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0Y2FzZSBjYy5tYWNyby5LRVkuZDpcblx0XHRcdFx0XHRcdFx0XHR0aGlzLmFjY1JpZ2h0ID0gZmFsc2U7XG5cdFx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdH1cblx0XHR9LFxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgb25Mb2FkOiBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdC8vIOWIneWni+WMlui3s+i3g+WKqOS9nFxuXHRcdFx0XHR0aGlzLmp1bXBBY3Rpb24gPSB0aGlzLnNldEp1bXBBY3Rpb24oKTtcblx0XHRcdFx0dGhpcy5ub2RlLnJ1bkFjdGlvbih0aGlzLmp1bXBBY3Rpb24pO1xuXHRcdFx0XHQvLyDliqDpgJ/luqbmlrnlkJHlvIDlhbNcblx0XHRcdFx0dGhpcy5hY2NMZWZ0ID0gZmFsc2U7XG5cdFx0XHRcdHRoaXMuYWNjUmlnaHQgPSBmYWxzZTtcblx0XHRcdFx0Ly8g5Li76KeS5b2T5YmN5rC05bmz5pa55ZCR6YCf5bqmXG5cdFx0XHRcdHRoaXMueFNwZWVkID0gMDtcblxuXHRcdFx0XHQvLyDliJ3lp4vljJbplK7nm5jovpPlhaXnm5HlkKxcblx0XHRcdFx0Y2Muc3lzdGVtRXZlbnQub24oY2MuU3lzdGVtRXZlbnQuRXZlbnRUeXBlLktFWV9ET1dOLCB0aGlzLm9uS2V5RG93biwgdGhpcyk7XG5cdFx0XHRcdGNjLnN5c3RlbUV2ZW50Lm9uKGNjLlN5c3RlbUV2ZW50LkV2ZW50VHlwZS5LRVlfVVAsIHRoaXMub25LZXlVcCwgdGhpcyk7XG5cdFx0fSxcblx0XHRvbkRlc3Ryb3kgKCkge1xuXHRcdFx0XHQvLyDlj5bmtojplK7nm5jovpPlhaXnm5HlkKxcblx0XHRcdFx0Y2Muc3lzdGVtRXZlbnQub2ZmKGNjLlN5c3RlbUV2ZW50LkV2ZW50VHlwZS5LRVlfRE9XTiwgdGhpcy5vbktleURvd24sIHRoaXMpO1xuXHRcdFx0XHRjYy5zeXN0ZW1FdmVudC5vZmYoY2MuU3lzdGVtRXZlbnQuRXZlbnRUeXBlLktFWV9VUCwgdGhpcy5vbktleVVwLCB0aGlzKTtcblx0XHR9LFxuICAgIHN0YXJ0ICgpIHtcblxuICAgIH0sXG5cdFx0dXBkYXRlOiBmdW5jdGlvbiAoZHQpIHtcblx0XHRcdFx0Ly8g5qC55o2u5b2T5YmN5Yqg6YCf5bqm5pa55ZCR5q+P5bin5pu05paw6YCf5bqmXG5cdFx0XHRcdGlmICh0aGlzLmFjY0xlZnQpIHtcblx0XHRcdFx0XHRcdHRoaXMueFNwZWVkIC09IHRoaXMuYWNjZWwgKiBkdDtcblx0XHRcdFx0fSBlbHNlIGlmICh0aGlzLmFjY1JpZ2h0KSB7XG5cdFx0XHRcdFx0XHR0aGlzLnhTcGVlZCArPSB0aGlzLmFjY2VsICogZHQ7XG5cdFx0XHRcdH1cblx0XHRcdFx0Ly8g6ZmQ5Yi25Li76KeS55qE6YCf5bqm5LiN6IO96LaF6L+H5pyA5aSn5YC8XG5cdFx0XHRcdGlmICggTWF0aC5hYnModGhpcy54U3BlZWQpID4gdGhpcy5tYXhNb3ZlU3BlZWQgKSB7XG5cdFx0XHRcdFx0XHQvLyBpZiBzcGVlZCByZWFjaCBsaW1pdCwgdXNlIG1heCBzcGVlZCB3aXRoIGN1cnJlbnQgZGlyZWN0aW9uXG5cdFx0XHRcdFx0XHR0aGlzLnhTcGVlZCA9IHRoaXMubWF4TW92ZVNwZWVkICogdGhpcy54U3BlZWQgLyBNYXRoLmFicyh0aGlzLnhTcGVlZCk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHQvLyDmoLnmja7lvZPliY3pgJ/luqbmm7TmlrDkuLvop5LnmoTkvY3nva5cblx0XHRcdFx0dGhpcy5ub2RlLnggKz0gdGhpcy54U3BlZWQgKiBkdDtcblx0XHR9LFxuXG4gICAgLy8gdXBkYXRlIChkdCkge30sXG59KTtcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Game.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '596486f/DRBDqrLAB0rPT1F', 'Game');
// scripts/Game.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    // 这个属性引用了星星预制资源
    starPrefab: {
      "default": null,
      type: cc.Prefab
    },
    // 星星产生后消失时间的随机范围
    maxStarDuration: 0,
    minStarDuration: 0,
    // 地面节点，用于确定星星生成的高度
    ground: {
      "default": null,
      type: cc.Node
    },
    // player 节点，用于获取主角弹跳的高度，和控制主角行动开关
    player: {
      "default": null,
      type: cc.Node
    }
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    // 获取地平面的 y 轴坐标
    this.groundY = this.ground.y + this.ground.height / 2; // 生成一个新的星星

    this.spawnNewStar();
  },
  spawnNewStar: function spawnNewStar() {
    // 使用给定的模板在场景中生成一个新节点
    var newStar = cc.instantiate(this.starPrefab); // 将新增的节点添加到 Canvas 节点下面

    this.node.addChild(newStar); // 为星星设置一个随机位置

    newStar.setPosition(this.getNewStarPosition());
  },
  getNewStarPosition: function getNewStarPosition() {
    var randX = 0; // 根据地平面位置和主角跳跃高度，随机得到一个星星的 y 坐标

    var randY = this.groundY + Math.random() * this.player.getComponent('Player').jumpHeight + 50; // 根据屏幕宽度，随机得到一个星星 x 坐标

    var maxX = this.node.width / 2;
    randX = (Math.random() - 0.5) * 2 * maxX; // 返回星星坐标

    return cc.v2(randX, randY);
  } // start () {
  // },
  // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL0dhbWUuanMiXSwibmFtZXMiOlsiY2MiLCJDbGFzcyIsIkNvbXBvbmVudCIsInByb3BlcnRpZXMiLCJzdGFyUHJlZmFiIiwidHlwZSIsIlByZWZhYiIsIm1heFN0YXJEdXJhdGlvbiIsIm1pblN0YXJEdXJhdGlvbiIsImdyb3VuZCIsIk5vZGUiLCJwbGF5ZXIiLCJvbkxvYWQiLCJncm91bmRZIiwieSIsImhlaWdodCIsInNwYXduTmV3U3RhciIsIm5ld1N0YXIiLCJpbnN0YW50aWF0ZSIsIm5vZGUiLCJhZGRDaGlsZCIsInNldFBvc2l0aW9uIiwiZ2V0TmV3U3RhclBvc2l0aW9uIiwicmFuZFgiLCJyYW5kWSIsIk1hdGgiLCJyYW5kb20iLCJnZXRDb21wb25lbnQiLCJqdW1wSGVpZ2h0IiwibWF4WCIsIndpZHRoIiwidjIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUFBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ0wsYUFBU0QsRUFBRSxDQUFDRSxTQURQO0FBR0xDLEVBQUFBLFVBQVUsRUFBRTtBQUNaO0FBQ0FDLElBQUFBLFVBQVUsRUFBRTtBQUNWLGlCQUFTLElBREM7QUFFVkMsTUFBQUEsSUFBSSxFQUFFTCxFQUFFLENBQUNNO0FBRkMsS0FGQTtBQU1aO0FBQ0FDLElBQUFBLGVBQWUsRUFBRSxDQVBMO0FBUVpDLElBQUFBLGVBQWUsRUFBRSxDQVJMO0FBU1o7QUFDQUMsSUFBQUEsTUFBTSxFQUFFO0FBQ04saUJBQVMsSUFESDtBQUVOSixNQUFBQSxJQUFJLEVBQUVMLEVBQUUsQ0FBQ1U7QUFGSCxLQVZJO0FBY1o7QUFDQUMsSUFBQUEsTUFBTSxFQUFFO0FBQ04saUJBQVMsSUFESDtBQUVOTixNQUFBQSxJQUFJLEVBQUVMLEVBQUUsQ0FBQ1U7QUFGSDtBQWZJLEdBSFA7QUF3Qkw7QUFFQUUsRUFBQUEsTUFBTSxFQUFFLGtCQUFZO0FBQ3BCO0FBQ0EsU0FBS0MsT0FBTCxHQUFlLEtBQUtKLE1BQUwsQ0FBWUssQ0FBWixHQUFnQixLQUFLTCxNQUFMLENBQVlNLE1BQVosR0FBbUIsQ0FBbEQsQ0FGb0IsQ0FHcEI7O0FBQ0EsU0FBS0MsWUFBTDtBQUNELEdBL0JNO0FBaUNQQSxFQUFBQSxZQUFZLEVBQUUsd0JBQVc7QUFDdkI7QUFDQSxRQUFJQyxPQUFPLEdBQUdqQixFQUFFLENBQUNrQixXQUFILENBQWUsS0FBS2QsVUFBcEIsQ0FBZCxDQUZ1QixDQUd2Qjs7QUFDQSxTQUFLZSxJQUFMLENBQVVDLFFBQVYsQ0FBbUJILE9BQW5CLEVBSnVCLENBS3ZCOztBQUNBQSxJQUFBQSxPQUFPLENBQUNJLFdBQVIsQ0FBb0IsS0FBS0Msa0JBQUwsRUFBcEI7QUFDRCxHQXhDTTtBQTBDUEEsRUFBQUEsa0JBQWtCLEVBQUUsOEJBQVk7QUFDOUIsUUFBSUMsS0FBSyxHQUFHLENBQVosQ0FEOEIsQ0FFOUI7O0FBQ0EsUUFBSUMsS0FBSyxHQUFHLEtBQUtYLE9BQUwsR0FBZVksSUFBSSxDQUFDQyxNQUFMLEtBQWdCLEtBQUtmLE1BQUwsQ0FBWWdCLFlBQVosQ0FBeUIsUUFBekIsRUFBbUNDLFVBQWxFLEdBQStFLEVBQTNGLENBSDhCLENBSTlCOztBQUNBLFFBQUlDLElBQUksR0FBRyxLQUFLVixJQUFMLENBQVVXLEtBQVYsR0FBZ0IsQ0FBM0I7QUFDQVAsSUFBQUEsS0FBSyxHQUFHLENBQUNFLElBQUksQ0FBQ0MsTUFBTCxLQUFnQixHQUFqQixJQUF3QixDQUF4QixHQUE0QkcsSUFBcEMsQ0FOOEIsQ0FPOUI7O0FBQ0EsV0FBTzdCLEVBQUUsQ0FBQytCLEVBQUgsQ0FBTVIsS0FBTixFQUFhQyxLQUFiLENBQVA7QUFDRCxHQW5ETSxDQXNETDtBQUVBO0FBRUE7O0FBMURLLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIGNjLkNsYXNzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvY2xhc3MuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmNjLkNsYXNzKHtcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXG5cbiAgICBwcm9wZXJ0aWVzOiB7XG5cdFx0XHRcdC8vIOi/meS4quWxnuaAp+W8leeUqOS6huaYn+aYn+mihOWItui1hOa6kFxuXHRcdFx0XHRzdGFyUHJlZmFiOiB7XG5cdFx0XHRcdFx0XHRkZWZhdWx0OiBudWxsLFxuXHRcdFx0XHRcdFx0dHlwZTogY2MuUHJlZmFiXG5cdFx0XHRcdH0sXG5cdFx0XHRcdC8vIOaYn+aYn+S6p+eUn+WQjua2iOWkseaXtumXtOeahOmaj+acuuiMg+WbtFxuXHRcdFx0XHRtYXhTdGFyRHVyYXRpb246IDAsXG5cdFx0XHRcdG1pblN0YXJEdXJhdGlvbjogMCxcblx0XHRcdFx0Ly8g5Zyw6Z2i6IqC54K577yM55So5LqO56Gu5a6a5pif5pif55Sf5oiQ55qE6auY5bqmXG5cdFx0XHRcdGdyb3VuZDoge1xuXHRcdFx0XHRcdFx0ZGVmYXVsdDogbnVsbCxcblx0XHRcdFx0XHRcdHR5cGU6IGNjLk5vZGVcblx0XHRcdFx0fSxcblx0XHRcdFx0Ly8gcGxheWVyIOiKgueCue+8jOeUqOS6juiOt+WPluS4u+inkuW8uei3s+eahOmrmOW6pu+8jOWSjOaOp+WItuS4u+inkuihjOWKqOW8gOWFs1xuXHRcdFx0XHRwbGF5ZXI6IHtcblx0XHRcdFx0XHRcdGRlZmF1bHQ6IG51bGwsXG5cdFx0XHRcdFx0XHR0eXBlOiBjYy5Ob2RlXG5cdFx0XHRcdH1cblx0XHR9LFxuXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XG5cbiAgICBvbkxvYWQ6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0Ly8g6I635Y+W5Zyw5bmz6Z2i55qEIHkg6L205Z2Q5qCHXG5cdFx0XHRcdHRoaXMuZ3JvdW5kWSA9IHRoaXMuZ3JvdW5kLnkgKyB0aGlzLmdyb3VuZC5oZWlnaHQvMjtcblx0XHRcdFx0Ly8g55Sf5oiQ5LiA5Liq5paw55qE5pif5pifXG5cdFx0XHRcdHRoaXMuc3Bhd25OZXdTdGFyKCk7XG5cdFx0fSxcblxuXHRcdHNwYXduTmV3U3RhcjogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdC8vIOS9v+eUqOe7meWumueahOaooeadv+WcqOWcuuaZr+S4reeUn+aIkOS4gOS4quaWsOiKgueCuVxuXHRcdFx0XHR2YXIgbmV3U3RhciA9IGNjLmluc3RhbnRpYXRlKHRoaXMuc3RhclByZWZhYik7XG5cdFx0XHRcdC8vIOWwhuaWsOWinueahOiKgueCuea3u+WKoOWIsCBDYW52YXMg6IqC54K55LiL6Z2iXG5cdFx0XHRcdHRoaXMubm9kZS5hZGRDaGlsZChuZXdTdGFyKTtcblx0XHRcdFx0Ly8g5Li65pif5pif6K6+572u5LiA5Liq6ZqP5py65L2N572uXG5cdFx0XHRcdG5ld1N0YXIuc2V0UG9zaXRpb24odGhpcy5nZXROZXdTdGFyUG9zaXRpb24oKSk7XG5cdFx0fSxcblxuXHRcdGdldE5ld1N0YXJQb3NpdGlvbjogZnVuY3Rpb24gKCkge1xuXHRcdFx0XHR2YXIgcmFuZFggPSAwO1xuXHRcdFx0XHQvLyDmoLnmja7lnLDlubPpnaLkvY3nva7lkozkuLvop5Lot7Pot4Ppq5jluqbvvIzpmo/mnLrlvpfliLDkuIDkuKrmmJ/mmJ/nmoQgeSDlnZDmoIdcblx0XHRcdFx0dmFyIHJhbmRZID0gdGhpcy5ncm91bmRZICsgTWF0aC5yYW5kb20oKSAqIHRoaXMucGxheWVyLmdldENvbXBvbmVudCgnUGxheWVyJykuanVtcEhlaWdodCArIDUwO1xuXHRcdFx0XHQvLyDmoLnmja7lsY/luZXlrr3luqbvvIzpmo/mnLrlvpfliLDkuIDkuKrmmJ/mmJ8geCDlnZDmoIdcblx0XHRcdFx0dmFyIG1heFggPSB0aGlzLm5vZGUud2lkdGgvMjtcblx0XHRcdFx0cmFuZFggPSAoTWF0aC5yYW5kb20oKSAtIDAuNSkgKiAyICogbWF4WDtcblx0XHRcdFx0Ly8g6L+U5Zue5pif5pif5Z2Q5qCHXG5cdFx0XHRcdHJldHVybiBjYy52MihyYW5kWCwgcmFuZFkpO1xuXHRcdH0sXG5cblxuICAgIC8vIHN0YXJ0ICgpIHtcblxuICAgIC8vIH0sXG5cbiAgICAvLyB1cGRhdGUgKGR0KSB7fSxcbn0pO1xuIl19
//------QC-SOURCE-SPLIT------
