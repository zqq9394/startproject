
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Player.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'f9223NU/XBFkJcxonbQhkp1', 'Player');
// scripts/Player.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
//cc 是 Cocos 的简称，Cocos 引擎的主要命名空间，引擎代码中所有的类、函数、属性和常量都在这个命名空间中定义
cc.Class({
  "extends": cc.Component,
  properties: {
    // 主角跳跃高度
    jumpHeight: 100,
    // 主角跳跃持续时间
    jumpDuration: 0.3,
    // 最大移动速度
    maxMoveSpeed: 400,
    // 加速度
    accel: 1000
  },
  setJumpAction: function setJumpAction() {
    // 跳跃上升
    //moveBy() 方法的作用是在规定的时间内移动指定的一段距离，第一个参数就是我们之前定义主角属性中的跳跃时间，第二个参数是一个 Vec2（表示 2D 向量和坐标）类型的对象，
    var jumpUp = cc.moveBy(this.jumpDuration, cc.v2(0, this.jumpHeight)).easing(cc.easeCubicActionOut()); // 下落

    var jumpDown = cc.moveBy(this.jumpDuration, cc.v2(0, -this.jumpHeight)).easing(cc.easeCubicActionIn()); // 不断重复

    return cc.repeatForever(cc.sequence(jumpUp, jumpDown));
  },
  onKeyDown: function onKeyDown(event) {
    // set a flag when key pressed
    switch (event.keyCode) {
      case cc.macro.KEY.a:
        this.accLeft = true;
        break;

      case cc.macro.KEY.d:
        this.accRight = true;
        break;
    }
  },
  onKeyUp: function onKeyUp(event) {
    // unset a flag when key released
    switch (event.keyCode) {
      case cc.macro.KEY.a:
        this.accLeft = false;
        break;

      case cc.macro.KEY.d:
        this.accRight = false;
        break;
    }
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    // 初始化跳跃动作
    this.jumpAction = this.setJumpAction();
    this.node.runAction(this.jumpAction); // 加速度方向开关

    this.accLeft = false;
    this.accRight = false; // 主角当前水平方向速度

    this.xSpeed = 0; // 初始化键盘输入监听

    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
  },
  onDestroy: function onDestroy() {
    // 取消键盘输入监听
    cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
  },
  start: function start() {},
  update: function update(dt) {
    // 根据当前加速度方向每帧更新速度
    if (this.accLeft) {
      this.xSpeed -= this.accel * dt;
    } else if (this.accRight) {
      this.xSpeed += this.accel * dt;
    } // 限制主角的速度不能超过最大值


    if (Math.abs(this.xSpeed) > this.maxMoveSpeed) {
      // if speed reach limit, use max speed with current direction
      this.xSpeed = this.maxMoveSpeed * this.xSpeed / Math.abs(this.xSpeed);
    } // 根据当前速度更新主角的位置


    this.node.x += this.xSpeed * dt;
  } // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL1BsYXllci5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsImp1bXBIZWlnaHQiLCJqdW1wRHVyYXRpb24iLCJtYXhNb3ZlU3BlZWQiLCJhY2NlbCIsInNldEp1bXBBY3Rpb24iLCJqdW1wVXAiLCJtb3ZlQnkiLCJ2MiIsImVhc2luZyIsImVhc2VDdWJpY0FjdGlvbk91dCIsImp1bXBEb3duIiwiZWFzZUN1YmljQWN0aW9uSW4iLCJyZXBlYXRGb3JldmVyIiwic2VxdWVuY2UiLCJvbktleURvd24iLCJldmVudCIsImtleUNvZGUiLCJtYWNybyIsIktFWSIsImEiLCJhY2NMZWZ0IiwiZCIsImFjY1JpZ2h0Iiwib25LZXlVcCIsIm9uTG9hZCIsImp1bXBBY3Rpb24iLCJub2RlIiwicnVuQWN0aW9uIiwieFNwZWVkIiwic3lzdGVtRXZlbnQiLCJvbiIsIlN5c3RlbUV2ZW50IiwiRXZlbnRUeXBlIiwiS0VZX0RPV04iLCJLRVlfVVAiLCJvbkRlc3Ryb3kiLCJvZmYiLCJzdGFydCIsInVwZGF0ZSIsImR0IiwiTWF0aCIsImFicyIsIngiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFO0FBQ1o7QUFDQUMsSUFBQUEsVUFBVSxFQUFFLEdBRkE7QUFHWjtBQUNBQyxJQUFBQSxZQUFZLEVBQUUsR0FKRjtBQUtaO0FBQ0FDLElBQUFBLFlBQVksRUFBRSxHQU5GO0FBT1o7QUFDQUMsSUFBQUEsS0FBSyxFQUFFO0FBUkssR0FIUDtBQWFQQyxFQUFBQSxhQUFhLEVBQUUseUJBQVk7QUFDekI7QUFDQTtBQUNBLFFBQUlDLE1BQU0sR0FBR1QsRUFBRSxDQUFDVSxNQUFILENBQVUsS0FBS0wsWUFBZixFQUE2QkwsRUFBRSxDQUFDVyxFQUFILENBQU0sQ0FBTixFQUFTLEtBQUtQLFVBQWQsQ0FBN0IsRUFBd0RRLE1BQXhELENBQStEWixFQUFFLENBQUNhLGtCQUFILEVBQS9ELENBQWIsQ0FIeUIsQ0FJekI7O0FBQ0EsUUFBSUMsUUFBUSxHQUFHZCxFQUFFLENBQUNVLE1BQUgsQ0FBVSxLQUFLTCxZQUFmLEVBQTZCTCxFQUFFLENBQUNXLEVBQUgsQ0FBTSxDQUFOLEVBQVMsQ0FBQyxLQUFLUCxVQUFmLENBQTdCLEVBQXlEUSxNQUF6RCxDQUFnRVosRUFBRSxDQUFDZSxpQkFBSCxFQUFoRSxDQUFmLENBTHlCLENBTXpCOztBQUNBLFdBQU9mLEVBQUUsQ0FBQ2dCLGFBQUgsQ0FBaUJoQixFQUFFLENBQUNpQixRQUFILENBQVlSLE1BQVosRUFBb0JLLFFBQXBCLENBQWpCLENBQVA7QUFDRCxHQXJCTTtBQXNCUEksRUFBQUEsU0F0Qk8scUJBc0JJQyxLQXRCSixFQXNCVztBQUNoQjtBQUNBLFlBQU9BLEtBQUssQ0FBQ0MsT0FBYjtBQUNFLFdBQUtwQixFQUFFLENBQUNxQixLQUFILENBQVNDLEdBQVQsQ0FBYUMsQ0FBbEI7QUFDRSxhQUFLQyxPQUFMLEdBQWUsSUFBZjtBQUNBOztBQUNGLFdBQUt4QixFQUFFLENBQUNxQixLQUFILENBQVNDLEdBQVQsQ0FBYUcsQ0FBbEI7QUFDRSxhQUFLQyxRQUFMLEdBQWdCLElBQWhCO0FBQ0E7QUFOSjtBQVFELEdBaENNO0FBa0NQQyxFQUFBQSxPQWxDTyxtQkFrQ0VSLEtBbENGLEVBa0NTO0FBQ2Q7QUFDQSxZQUFPQSxLQUFLLENBQUNDLE9BQWI7QUFDRSxXQUFLcEIsRUFBRSxDQUFDcUIsS0FBSCxDQUFTQyxHQUFULENBQWFDLENBQWxCO0FBQ0UsYUFBS0MsT0FBTCxHQUFlLEtBQWY7QUFDQTs7QUFDRixXQUFLeEIsRUFBRSxDQUFDcUIsS0FBSCxDQUFTQyxHQUFULENBQWFHLENBQWxCO0FBQ0UsYUFBS0MsUUFBTCxHQUFnQixLQUFoQjtBQUNBO0FBTko7QUFRRCxHQTVDTTtBQTZDTDtBQUVBRSxFQUFBQSxNQUFNLEVBQUUsa0JBQVk7QUFDcEI7QUFDQSxTQUFLQyxVQUFMLEdBQWtCLEtBQUtyQixhQUFMLEVBQWxCO0FBQ0EsU0FBS3NCLElBQUwsQ0FBVUMsU0FBVixDQUFvQixLQUFLRixVQUF6QixFQUhvQixDQUlwQjs7QUFDQSxTQUFLTCxPQUFMLEdBQWUsS0FBZjtBQUNBLFNBQUtFLFFBQUwsR0FBZ0IsS0FBaEIsQ0FOb0IsQ0FPcEI7O0FBQ0EsU0FBS00sTUFBTCxHQUFjLENBQWQsQ0FSb0IsQ0FVcEI7O0FBQ0FoQyxJQUFBQSxFQUFFLENBQUNpQyxXQUFILENBQWVDLEVBQWYsQ0FBa0JsQyxFQUFFLENBQUNtQyxXQUFILENBQWVDLFNBQWYsQ0FBeUJDLFFBQTNDLEVBQXFELEtBQUtuQixTQUExRCxFQUFxRSxJQUFyRTtBQUNBbEIsSUFBQUEsRUFBRSxDQUFDaUMsV0FBSCxDQUFlQyxFQUFmLENBQWtCbEMsRUFBRSxDQUFDbUMsV0FBSCxDQUFlQyxTQUFmLENBQXlCRSxNQUEzQyxFQUFtRCxLQUFLWCxPQUF4RCxFQUFpRSxJQUFqRTtBQUNELEdBNURNO0FBNkRQWSxFQUFBQSxTQTdETyx1QkE2RE07QUFDWDtBQUNBdkMsSUFBQUEsRUFBRSxDQUFDaUMsV0FBSCxDQUFlTyxHQUFmLENBQW1CeEMsRUFBRSxDQUFDbUMsV0FBSCxDQUFlQyxTQUFmLENBQXlCQyxRQUE1QyxFQUFzRCxLQUFLbkIsU0FBM0QsRUFBc0UsSUFBdEU7QUFDQWxCLElBQUFBLEVBQUUsQ0FBQ2lDLFdBQUgsQ0FBZU8sR0FBZixDQUFtQnhDLEVBQUUsQ0FBQ21DLFdBQUgsQ0FBZUMsU0FBZixDQUF5QkUsTUFBNUMsRUFBb0QsS0FBS1gsT0FBekQsRUFBa0UsSUFBbEU7QUFDRCxHQWpFTTtBQWtFTGMsRUFBQUEsS0FsRUssbUJBa0VJLENBRVIsQ0FwRUk7QUFxRVBDLEVBQUFBLE1BQU0sRUFBRSxnQkFBVUMsRUFBVixFQUFjO0FBQ3BCO0FBQ0EsUUFBSSxLQUFLbkIsT0FBVCxFQUFrQjtBQUNoQixXQUFLUSxNQUFMLElBQWUsS0FBS3pCLEtBQUwsR0FBYW9DLEVBQTVCO0FBQ0QsS0FGRCxNQUVPLElBQUksS0FBS2pCLFFBQVQsRUFBbUI7QUFDeEIsV0FBS00sTUFBTCxJQUFlLEtBQUt6QixLQUFMLEdBQWFvQyxFQUE1QjtBQUNELEtBTm1CLENBT3BCOzs7QUFDQSxRQUFLQyxJQUFJLENBQUNDLEdBQUwsQ0FBUyxLQUFLYixNQUFkLElBQXdCLEtBQUsxQixZQUFsQyxFQUFpRDtBQUMvQztBQUNBLFdBQUswQixNQUFMLEdBQWMsS0FBSzFCLFlBQUwsR0FBb0IsS0FBSzBCLE1BQXpCLEdBQWtDWSxJQUFJLENBQUNDLEdBQUwsQ0FBUyxLQUFLYixNQUFkLENBQWhEO0FBQ0QsS0FYbUIsQ0FhcEI7OztBQUNBLFNBQUtGLElBQUwsQ0FBVWdCLENBQVYsSUFBZSxLQUFLZCxNQUFMLEdBQWNXLEVBQTdCO0FBQ0QsR0FwRk0sQ0FzRkw7O0FBdEZLLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIGNjLkNsYXNzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvY2xhc3MuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG4vL2NjIOaYryBDb2NvcyDnmoTnroDnp7DvvIxDb2NvcyDlvJXmk47nmoTkuLvopoHlkb3lkI3nqbrpl7TvvIzlvJXmk47ku6PnoIHkuK3miYDmnInnmoTnsbvjgIHlh73mlbDjgIHlsZ7mgKflkozluLjph4/pg73lnKjov5nkuKrlkb3lkI3nqbrpl7TkuK3lrprkuYlcbmNjLkNsYXNzKHtcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXG5cbiAgICBwcm9wZXJ0aWVzOiB7XG5cdFx0XHRcdC8vIOS4u+inkui3s+i3g+mrmOW6plxuXHRcdFx0XHRqdW1wSGVpZ2h0OiAxMDAsXG5cdFx0XHRcdC8vIOS4u+inkui3s+i3g+aMgee7reaXtumXtFxuXHRcdFx0XHRqdW1wRHVyYXRpb246IDAuMyxcblx0XHRcdFx0Ly8g5pyA5aSn56e75Yqo6YCf5bqmXG5cdFx0XHRcdG1heE1vdmVTcGVlZDogNDAwLFxuXHRcdFx0XHQvLyDliqDpgJ/luqZcblx0XHRcdFx0YWNjZWw6IDEwMDAsXG5cdFx0fSxcblx0XHRzZXRKdW1wQWN0aW9uOiBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdC8vIOi3s+i3g+S4iuWNh1xuXHRcdFx0XHQvL21vdmVCeSgpIOaWueazleeahOS9nOeUqOaYr+WcqOinhOWumueahOaXtumXtOWGheenu+WKqOaMh+WumueahOS4gOautei3neemu++8jOesrOS4gOS4quWPguaVsOWwseaYr+aIkeS7rOS5i+WJjeWumuS5ieS4u+inkuWxnuaAp+S4reeahOi3s+i3g+aXtumXtO+8jOesrOS6jOS4quWPguaVsOaYr+S4gOS4qiBWZWMy77yI6KGo56S6IDJEIOWQkemHj+WSjOWdkOagh++8ieexu+Wei+eahOWvueixoe+8jFxuXHRcdFx0XHR2YXIganVtcFVwID0gY2MubW92ZUJ5KHRoaXMuanVtcER1cmF0aW9uLCBjYy52MigwLCB0aGlzLmp1bXBIZWlnaHQpKS5lYXNpbmcoY2MuZWFzZUN1YmljQWN0aW9uT3V0KCkpO1xuXHRcdFx0XHQvLyDkuIvokL1cblx0XHRcdFx0dmFyIGp1bXBEb3duID0gY2MubW92ZUJ5KHRoaXMuanVtcER1cmF0aW9uLCBjYy52MigwLCAtdGhpcy5qdW1wSGVpZ2h0KSkuZWFzaW5nKGNjLmVhc2VDdWJpY0FjdGlvbkluKCkpO1xuXHRcdFx0XHQvLyDkuI3mlq3ph43lpI1cblx0XHRcdFx0cmV0dXJuIGNjLnJlcGVhdEZvcmV2ZXIoY2Muc2VxdWVuY2UoanVtcFVwLCBqdW1wRG93bikpO1xuXHRcdH0sXG5cdFx0b25LZXlEb3duIChldmVudCkge1xuXHRcdFx0XHQvLyBzZXQgYSBmbGFnIHdoZW4ga2V5IHByZXNzZWRcblx0XHRcdFx0c3dpdGNoKGV2ZW50LmtleUNvZGUpIHtcblx0XHRcdFx0XHRcdGNhc2UgY2MubWFjcm8uS0VZLmE6XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5hY2NMZWZ0ID0gdHJ1ZTtcblx0XHRcdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRcdGNhc2UgY2MubWFjcm8uS0VZLmQ6XG5cdFx0XHRcdFx0XHRcdFx0dGhpcy5hY2NSaWdodCA9IHRydWU7XG5cdFx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdH1cblx0XHR9LFxuXG5cdFx0b25LZXlVcCAoZXZlbnQpIHtcblx0XHRcdFx0Ly8gdW5zZXQgYSBmbGFnIHdoZW4ga2V5IHJlbGVhc2VkXG5cdFx0XHRcdHN3aXRjaChldmVudC5rZXlDb2RlKSB7XG5cdFx0XHRcdFx0XHRjYXNlIGNjLm1hY3JvLktFWS5hOlxuXHRcdFx0XHRcdFx0XHRcdHRoaXMuYWNjTGVmdCA9IGZhbHNlO1xuXHRcdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0Y2FzZSBjYy5tYWNyby5LRVkuZDpcblx0XHRcdFx0XHRcdFx0XHR0aGlzLmFjY1JpZ2h0ID0gZmFsc2U7XG5cdFx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdH1cblx0XHR9LFxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuXG4gICAgb25Mb2FkOiBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdC8vIOWIneWni+WMlui3s+i3g+WKqOS9nFxuXHRcdFx0XHR0aGlzLmp1bXBBY3Rpb24gPSB0aGlzLnNldEp1bXBBY3Rpb24oKTtcblx0XHRcdFx0dGhpcy5ub2RlLnJ1bkFjdGlvbih0aGlzLmp1bXBBY3Rpb24pO1xuXHRcdFx0XHQvLyDliqDpgJ/luqbmlrnlkJHlvIDlhbNcblx0XHRcdFx0dGhpcy5hY2NMZWZ0ID0gZmFsc2U7XG5cdFx0XHRcdHRoaXMuYWNjUmlnaHQgPSBmYWxzZTtcblx0XHRcdFx0Ly8g5Li76KeS5b2T5YmN5rC05bmz5pa55ZCR6YCf5bqmXG5cdFx0XHRcdHRoaXMueFNwZWVkID0gMDtcblxuXHRcdFx0XHQvLyDliJ3lp4vljJbplK7nm5jovpPlhaXnm5HlkKxcblx0XHRcdFx0Y2Muc3lzdGVtRXZlbnQub24oY2MuU3lzdGVtRXZlbnQuRXZlbnRUeXBlLktFWV9ET1dOLCB0aGlzLm9uS2V5RG93biwgdGhpcyk7XG5cdFx0XHRcdGNjLnN5c3RlbUV2ZW50Lm9uKGNjLlN5c3RlbUV2ZW50LkV2ZW50VHlwZS5LRVlfVVAsIHRoaXMub25LZXlVcCwgdGhpcyk7XG5cdFx0fSxcblx0XHRvbkRlc3Ryb3kgKCkge1xuXHRcdFx0XHQvLyDlj5bmtojplK7nm5jovpPlhaXnm5HlkKxcblx0XHRcdFx0Y2Muc3lzdGVtRXZlbnQub2ZmKGNjLlN5c3RlbUV2ZW50LkV2ZW50VHlwZS5LRVlfRE9XTiwgdGhpcy5vbktleURvd24sIHRoaXMpO1xuXHRcdFx0XHRjYy5zeXN0ZW1FdmVudC5vZmYoY2MuU3lzdGVtRXZlbnQuRXZlbnRUeXBlLktFWV9VUCwgdGhpcy5vbktleVVwLCB0aGlzKTtcblx0XHR9LFxuICAgIHN0YXJ0ICgpIHtcblxuICAgIH0sXG5cdFx0dXBkYXRlOiBmdW5jdGlvbiAoZHQpIHtcblx0XHRcdFx0Ly8g5qC55o2u5b2T5YmN5Yqg6YCf5bqm5pa55ZCR5q+P5bin5pu05paw6YCf5bqmXG5cdFx0XHRcdGlmICh0aGlzLmFjY0xlZnQpIHtcblx0XHRcdFx0XHRcdHRoaXMueFNwZWVkIC09IHRoaXMuYWNjZWwgKiBkdDtcblx0XHRcdFx0fSBlbHNlIGlmICh0aGlzLmFjY1JpZ2h0KSB7XG5cdFx0XHRcdFx0XHR0aGlzLnhTcGVlZCArPSB0aGlzLmFjY2VsICogZHQ7XG5cdFx0XHRcdH1cblx0XHRcdFx0Ly8g6ZmQ5Yi25Li76KeS55qE6YCf5bqm5LiN6IO96LaF6L+H5pyA5aSn5YC8XG5cdFx0XHRcdGlmICggTWF0aC5hYnModGhpcy54U3BlZWQpID4gdGhpcy5tYXhNb3ZlU3BlZWQgKSB7XG5cdFx0XHRcdFx0XHQvLyBpZiBzcGVlZCByZWFjaCBsaW1pdCwgdXNlIG1heCBzcGVlZCB3aXRoIGN1cnJlbnQgZGlyZWN0aW9uXG5cdFx0XHRcdFx0XHR0aGlzLnhTcGVlZCA9IHRoaXMubWF4TW92ZVNwZWVkICogdGhpcy54U3BlZWQgLyBNYXRoLmFicyh0aGlzLnhTcGVlZCk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHQvLyDmoLnmja7lvZPliY3pgJ/luqbmm7TmlrDkuLvop5LnmoTkvY3nva5cblx0XHRcdFx0dGhpcy5ub2RlLnggKz0gdGhpcy54U3BlZWQgKiBkdDtcblx0XHR9LFxuXG4gICAgLy8gdXBkYXRlIChkdCkge30sXG59KTtcbiJdfQ==