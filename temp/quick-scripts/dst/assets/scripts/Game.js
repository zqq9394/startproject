
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Game.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '596486f/DRBDqrLAB0rPT1F', 'Game');
// scripts/Game.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    // 这个属性引用了星星预制资源
    starPrefab: {
      "default": null,
      type: cc.Prefab
    },
    // 星星产生后消失时间的随机范围
    maxStarDuration: 0,
    minStarDuration: 0,
    // 地面节点，用于确定星星生成的高度
    ground: {
      "default": null,
      type: cc.Node
    },
    // player 节点，用于获取主角弹跳的高度，和控制主角行动开关
    player: {
      "default": null,
      type: cc.Node
    }
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    // 获取地平面的 y 轴坐标
    this.groundY = this.ground.y + this.ground.height / 2; // 生成一个新的星星

    this.spawnNewStar();
  },
  spawnNewStar: function spawnNewStar() {
    // 使用给定的模板在场景中生成一个新节点
    var newStar = cc.instantiate(this.starPrefab); // 将新增的节点添加到 Canvas 节点下面

    this.node.addChild(newStar); // 为星星设置一个随机位置

    newStar.setPosition(this.getNewStarPosition());
  },
  getNewStarPosition: function getNewStarPosition() {
    var randX = 0; // 根据地平面位置和主角跳跃高度，随机得到一个星星的 y 坐标

    var randY = this.groundY + Math.random() * this.player.getComponent('Player').jumpHeight + 50; // 根据屏幕宽度，随机得到一个星星 x 坐标

    var maxX = this.node.width / 2;
    randX = (Math.random() - 0.5) * 2 * maxX; // 返回星星坐标

    return cc.v2(randX, randY);
  } // start () {
  // },
  // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL0dhbWUuanMiXSwibmFtZXMiOlsiY2MiLCJDbGFzcyIsIkNvbXBvbmVudCIsInByb3BlcnRpZXMiLCJzdGFyUHJlZmFiIiwidHlwZSIsIlByZWZhYiIsIm1heFN0YXJEdXJhdGlvbiIsIm1pblN0YXJEdXJhdGlvbiIsImdyb3VuZCIsIk5vZGUiLCJwbGF5ZXIiLCJvbkxvYWQiLCJncm91bmRZIiwieSIsImhlaWdodCIsInNwYXduTmV3U3RhciIsIm5ld1N0YXIiLCJpbnN0YW50aWF0ZSIsIm5vZGUiLCJhZGRDaGlsZCIsInNldFBvc2l0aW9uIiwiZ2V0TmV3U3RhclBvc2l0aW9uIiwicmFuZFgiLCJyYW5kWSIsIk1hdGgiLCJyYW5kb20iLCJnZXRDb21wb25lbnQiLCJqdW1wSGVpZ2h0IiwibWF4WCIsIndpZHRoIiwidjIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUFBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ0wsYUFBU0QsRUFBRSxDQUFDRSxTQURQO0FBR0xDLEVBQUFBLFVBQVUsRUFBRTtBQUNaO0FBQ0FDLElBQUFBLFVBQVUsRUFBRTtBQUNWLGlCQUFTLElBREM7QUFFVkMsTUFBQUEsSUFBSSxFQUFFTCxFQUFFLENBQUNNO0FBRkMsS0FGQTtBQU1aO0FBQ0FDLElBQUFBLGVBQWUsRUFBRSxDQVBMO0FBUVpDLElBQUFBLGVBQWUsRUFBRSxDQVJMO0FBU1o7QUFDQUMsSUFBQUEsTUFBTSxFQUFFO0FBQ04saUJBQVMsSUFESDtBQUVOSixNQUFBQSxJQUFJLEVBQUVMLEVBQUUsQ0FBQ1U7QUFGSCxLQVZJO0FBY1o7QUFDQUMsSUFBQUEsTUFBTSxFQUFFO0FBQ04saUJBQVMsSUFESDtBQUVOTixNQUFBQSxJQUFJLEVBQUVMLEVBQUUsQ0FBQ1U7QUFGSDtBQWZJLEdBSFA7QUF3Qkw7QUFFQUUsRUFBQUEsTUFBTSxFQUFFLGtCQUFZO0FBQ3BCO0FBQ0EsU0FBS0MsT0FBTCxHQUFlLEtBQUtKLE1BQUwsQ0FBWUssQ0FBWixHQUFnQixLQUFLTCxNQUFMLENBQVlNLE1BQVosR0FBbUIsQ0FBbEQsQ0FGb0IsQ0FHcEI7O0FBQ0EsU0FBS0MsWUFBTDtBQUNELEdBL0JNO0FBaUNQQSxFQUFBQSxZQUFZLEVBQUUsd0JBQVc7QUFDdkI7QUFDQSxRQUFJQyxPQUFPLEdBQUdqQixFQUFFLENBQUNrQixXQUFILENBQWUsS0FBS2QsVUFBcEIsQ0FBZCxDQUZ1QixDQUd2Qjs7QUFDQSxTQUFLZSxJQUFMLENBQVVDLFFBQVYsQ0FBbUJILE9BQW5CLEVBSnVCLENBS3ZCOztBQUNBQSxJQUFBQSxPQUFPLENBQUNJLFdBQVIsQ0FBb0IsS0FBS0Msa0JBQUwsRUFBcEI7QUFDRCxHQXhDTTtBQTBDUEEsRUFBQUEsa0JBQWtCLEVBQUUsOEJBQVk7QUFDOUIsUUFBSUMsS0FBSyxHQUFHLENBQVosQ0FEOEIsQ0FFOUI7O0FBQ0EsUUFBSUMsS0FBSyxHQUFHLEtBQUtYLE9BQUwsR0FBZVksSUFBSSxDQUFDQyxNQUFMLEtBQWdCLEtBQUtmLE1BQUwsQ0FBWWdCLFlBQVosQ0FBeUIsUUFBekIsRUFBbUNDLFVBQWxFLEdBQStFLEVBQTNGLENBSDhCLENBSTlCOztBQUNBLFFBQUlDLElBQUksR0FBRyxLQUFLVixJQUFMLENBQVVXLEtBQVYsR0FBZ0IsQ0FBM0I7QUFDQVAsSUFBQUEsS0FBSyxHQUFHLENBQUNFLElBQUksQ0FBQ0MsTUFBTCxLQUFnQixHQUFqQixJQUF3QixDQUF4QixHQUE0QkcsSUFBcEMsQ0FOOEIsQ0FPOUI7O0FBQ0EsV0FBTzdCLEVBQUUsQ0FBQytCLEVBQUgsQ0FBTVIsS0FBTixFQUFhQyxLQUFiLENBQVA7QUFDRCxHQW5ETSxDQXNETDtBQUVBO0FBRUE7O0FBMURLLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIGNjLkNsYXNzOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvY2xhc3MuaHRtbFxuLy8gTGVhcm4gQXR0cmlidXRlOlxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9saWZlLWN5Y2xlLWNhbGxiYWNrcy5odG1sXG5cbmNjLkNsYXNzKHtcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXG5cbiAgICBwcm9wZXJ0aWVzOiB7XG5cdFx0XHRcdC8vIOi/meS4quWxnuaAp+W8leeUqOS6huaYn+aYn+mihOWItui1hOa6kFxuXHRcdFx0XHRzdGFyUHJlZmFiOiB7XG5cdFx0XHRcdFx0XHRkZWZhdWx0OiBudWxsLFxuXHRcdFx0XHRcdFx0dHlwZTogY2MuUHJlZmFiXG5cdFx0XHRcdH0sXG5cdFx0XHRcdC8vIOaYn+aYn+S6p+eUn+WQjua2iOWkseaXtumXtOeahOmaj+acuuiMg+WbtFxuXHRcdFx0XHRtYXhTdGFyRHVyYXRpb246IDAsXG5cdFx0XHRcdG1pblN0YXJEdXJhdGlvbjogMCxcblx0XHRcdFx0Ly8g5Zyw6Z2i6IqC54K577yM55So5LqO56Gu5a6a5pif5pif55Sf5oiQ55qE6auY5bqmXG5cdFx0XHRcdGdyb3VuZDoge1xuXHRcdFx0XHRcdFx0ZGVmYXVsdDogbnVsbCxcblx0XHRcdFx0XHRcdHR5cGU6IGNjLk5vZGVcblx0XHRcdFx0fSxcblx0XHRcdFx0Ly8gcGxheWVyIOiKgueCue+8jOeUqOS6juiOt+WPluS4u+inkuW8uei3s+eahOmrmOW6pu+8jOWSjOaOp+WItuS4u+inkuihjOWKqOW8gOWFs1xuXHRcdFx0XHRwbGF5ZXI6IHtcblx0XHRcdFx0XHRcdGRlZmF1bHQ6IG51bGwsXG5cdFx0XHRcdFx0XHR0eXBlOiBjYy5Ob2RlXG5cdFx0XHRcdH1cblx0XHR9LFxuXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XG5cbiAgICBvbkxvYWQ6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0Ly8g6I635Y+W5Zyw5bmz6Z2i55qEIHkg6L205Z2Q5qCHXG5cdFx0XHRcdHRoaXMuZ3JvdW5kWSA9IHRoaXMuZ3JvdW5kLnkgKyB0aGlzLmdyb3VuZC5oZWlnaHQvMjtcblx0XHRcdFx0Ly8g55Sf5oiQ5LiA5Liq5paw55qE5pif5pifXG5cdFx0XHRcdHRoaXMuc3Bhd25OZXdTdGFyKCk7XG5cdFx0fSxcblxuXHRcdHNwYXduTmV3U3RhcjogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdC8vIOS9v+eUqOe7meWumueahOaooeadv+WcqOWcuuaZr+S4reeUn+aIkOS4gOS4quaWsOiKgueCuVxuXHRcdFx0XHR2YXIgbmV3U3RhciA9IGNjLmluc3RhbnRpYXRlKHRoaXMuc3RhclByZWZhYik7XG5cdFx0XHRcdC8vIOWwhuaWsOWinueahOiKgueCuea3u+WKoOWIsCBDYW52YXMg6IqC54K55LiL6Z2iXG5cdFx0XHRcdHRoaXMubm9kZS5hZGRDaGlsZChuZXdTdGFyKTtcblx0XHRcdFx0Ly8g5Li65pif5pif6K6+572u5LiA5Liq6ZqP5py65L2N572uXG5cdFx0XHRcdG5ld1N0YXIuc2V0UG9zaXRpb24odGhpcy5nZXROZXdTdGFyUG9zaXRpb24oKSk7XG5cdFx0fSxcblxuXHRcdGdldE5ld1N0YXJQb3NpdGlvbjogZnVuY3Rpb24gKCkge1xuXHRcdFx0XHR2YXIgcmFuZFggPSAwO1xuXHRcdFx0XHQvLyDmoLnmja7lnLDlubPpnaLkvY3nva7lkozkuLvop5Lot7Pot4Ppq5jluqbvvIzpmo/mnLrlvpfliLDkuIDkuKrmmJ/mmJ/nmoQgeSDlnZDmoIdcblx0XHRcdFx0dmFyIHJhbmRZID0gdGhpcy5ncm91bmRZICsgTWF0aC5yYW5kb20oKSAqIHRoaXMucGxheWVyLmdldENvbXBvbmVudCgnUGxheWVyJykuanVtcEhlaWdodCArIDUwO1xuXHRcdFx0XHQvLyDmoLnmja7lsY/luZXlrr3luqbvvIzpmo/mnLrlvpfliLDkuIDkuKrmmJ/mmJ8geCDlnZDmoIdcblx0XHRcdFx0dmFyIG1heFggPSB0aGlzLm5vZGUud2lkdGgvMjtcblx0XHRcdFx0cmFuZFggPSAoTWF0aC5yYW5kb20oKSAtIDAuNSkgKiAyICogbWF4WDtcblx0XHRcdFx0Ly8g6L+U5Zue5pif5pif5Z2Q5qCHXG5cdFx0XHRcdHJldHVybiBjYy52MihyYW5kWCwgcmFuZFkpO1xuXHRcdH0sXG5cblxuICAgIC8vIHN0YXJ0ICgpIHtcblxuICAgIC8vIH0sXG5cbiAgICAvLyB1cGRhdGUgKGR0KSB7fSxcbn0pO1xuIl19